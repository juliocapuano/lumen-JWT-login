<?php

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ResponseHelper;
use App\User;
use App\Jwt\JwtGenerator;
use Illuminate\Support\Facades\Storage;

class MessageTest extends TestCase
{    
    
    /**
     * User dataprovider
     * @return User
     */
    public function dataProviderUser()
    {
        return [
            [
                'user' => new User(
                    ['id' => '0', 'name' => 'Invalid', 'email' => 'invalid_user@gmail.com']
                )
            ]
        ];
    }    

    /**
     * Test POST /messages
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testAddMessageInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->post(
            '/messages',
            [                
                'message' => 'Hi, this is a message',
                'users_id_to' => 2,
                'items_id' => 1
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }

    /**
     * Test POST /messages
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testAddMessage(User $user)
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];   

        $response = $this->post(
            '/messages',
            [                
                'message' => 'Hi, this is a message',
                'users_id_to' => 2,
                'items_id' => 1
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('msg', $response);
    }

    /**
     * Test POST /messages
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testAddMessageInChain(User $user)
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];   

        $response = $this->post(
            '/messages',
            [                
                'message' => 'I forgot to thell you something, this is the thing, bla bla bla!',
                'users_id_to' => 2,
                'items_id' => 1,
                'parent_id' => '1_1'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('msg', $response);
    }

    /**
     * Test POST /messages
     * Share item to another user
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testAddMessageShareItem(User $user)
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];   

        $response = $this->post(
            '/messages',
            [                
                'message' => 'Hi, look, this is the item I told you last night',
                'users_id_to' => 2,
                'items_id' => 1
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('msg', $response);
    }

    /**
     * Test GET /messages
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testGetMyMessagesInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->get(
            '/messages',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }

    /**
     * Test GET /messages
     * @return void
     */
    public function testGetMyMessages()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user2@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        $response = $this->get(
            '/messages?items_id=1&parent_id=1_1',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('messages', $response);
        $this->assertEquals(3, count($response['messages']));
    }

    /**
     * Test GET /messagesresume
     * @return void
     */
    public function testGetMessagesResume()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        $response = $this->get(
            '/messagesresume',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('messages', $response);        
        $this->assertEquals(1, count($response['messages']));
    }
}
