<?php

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Helpers\ResponseHelper;
use App\User;
use App\Jwt\JwtGenerator;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ItemTest extends TestCase
{    
    
    /**
     * User dataprovider
     * @return User
     */
    public function dataProviderUser()
    {
        return [
            [
                'user' => new User(
                    ['id' => '0', 'name' => 'Invalid', 'email' => 'invalid_user@gmail.com']
                )
            ]
        ];
    }    

    /**
     * Test POST /items
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testAddItemInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->post(
            '/items',
            [                
                'title' => 'Title 1',
                'description' => 'Description 1'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }

    /**
     * Test POST /items
     * @return void
     */
    public function testAddItemWithoutFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];

        $response = $this->post(
            '/items',
            [                
                'title' => 'Title 1',
                'description' => 'Description 1'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('item_data', $response);
        $this->assertArrayHasKey('file_data', $response);
    }

    /**
     * Test POST /items
     * @return void
     */
    public function testAddItemWithFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        Storage::fake('testing');

        $file = UploadedFile::fake()->image('demo.jpg', 1024, 1024);

        $response = $this->post(
            '/items',
            [                
                'title' => 'Title 1',
                'description' => 'Description 1',                
                'image' => $file
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);           

        $this->assertArrayHasKey('item_data', $response);
        $this->assertArrayHasKey('file_data', $response);
    }

    /**
     * Test GET /items
     * @return void
     */
    public function testGetItemsNotLoggedIn()
    {        
        $response = $this->get(
            '/items'
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);            

        $this->assertArrayHasKey('items', $response);
    }

    /**
     * Test GET /items
     * @return void
     */
    public function testGetItemsLoggedIn()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        $response = $this->get(
            '/items',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);            

        $this->assertArrayHasKey('items', $response);
    }

    /**
     * Test GET /items
     * @return void
     */
    public function testGetSearchItemsNotLoggedIn()
    {        
        $response = $this->get(
            '/items',
            ['text'=>'itle']
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);
        $this->assertArrayHasKey('items', $response);
        $this->assertGreaterThan(1, $response['items']);
    }

    /**
     * Test GET /myitems
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testGetMyItemsInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->get(
            '/myitems',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }

    /**
     * Test GET /myitems
     * @return void
     */
    public function testGetMyItems()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        $response = $this->get(
            '/myitems',            
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);            

        $this->assertArrayHasKey('items', $response);
    }

    /**
     * Test GET /items
     * @return void
     */
    public function testGetSearchMyItems()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        $response = $this->get(
            '/myitems?text=itle',            
            ['text'=>'description', 'HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);        
        $this->assertArrayHasKey('items', $response);
        $this->assertGreaterThan(1, $response['items']);
    }

    /**
     * Test POST /edititems
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testUpdateItemsInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->post(
            '/edititems',
            [                
                'id' => '1',
                'title' => 'Title 1 updated',
                'description' => 'Description 1 updated'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }

    /**
     * Test POST /edititems
     * @return void
     */
    public function testUpdateItemWithoutFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];

        $response = $this->post(
            '/edititems',
            [                
                'id' => '1',
                'title' => 'Title 1 updated',
                'description' => 'Description 1 updated'
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('item_data', $response);
        $this->assertArrayHasKey('file_data', $response);
    }

    /**
     * Test POST /edititems
     * @return void
     */
    public function testUpdateItemWithFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        Storage::fake('testing');

        $file = UploadedFile::fake()->image('demo.jpg', 1024, 1024);

        $response = $this->post(
            '/edititems',
            [                
                'id' => '1',
                'title' => 'Title 1 updated',
                'description' => 'Description 1 updated',
                'image' => $file
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);           

        $this->assertArrayHasKey('item_data', $response);
        $this->assertArrayHasKey('file_data', $response);
    }

    /**
     * Test POST /itemupload
     * @dataProvider dataProviderUser
     * @return void
     */
    public function testUploadInvalidUser(User $user)
    {
        $token = JwtGenerator::generate($user, 60);

        $response = $this->post(
            '/itemupload',
            [   'image' => ''
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            400,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);          

        $this->assertArrayHasKey('error', $response);        
    }


    /**
     * Test POST /items
     * @return void
     */
    public function testUploadFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        Storage::fake('testing');

        $file = UploadedFile::fake()->image('demo.jpg', 1024, 1024);

        $response = $this->post(
            '/itemupload',
            [   'image' => $file
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);
        
        $this->assertArrayHasKey('file_data', $response);
    }

    /**
     * Test POST /items
     * @return void
     */
    public function testReplaceFile()
    {
        $this->post(
            '/auth/login',
            [
                'email' => 'test_user@gmail.com',
                'password' => '12345'
            ]
        );
        $response = $this->response->getContent();
        $token = json_decode($this->response->getContent(), true)['token'];        

        Storage::fake('testing');

        $file = UploadedFile::fake()->image('demo.jpg', 1024, 1024);

        $response = $this->post(
            '/itemupload',
            [   'image' => $file
            ],
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );

        $response = json_decode($this->response->getContent(),true);

        $this->assertArrayHasKey('file_data', $response);
    }
}
