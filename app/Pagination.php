<?php

namespace App;

class Pagination
{
    const ITEMS_PER_PAGE = 20;   
    const PAGINATION_URL = 'pagination';
}
