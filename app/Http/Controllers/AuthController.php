<?php
namespace App\Http\Controllers;

use Validator;
use App\User;
use App\Jwt\JwtGenerator;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller 
{
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user
     * @return mixed
     */
    public function authenticate(User $user) {
        
        $this->validate($this->request, User::VALIDATION_RULES);
        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();
        if (!$user) {
            return ResponseHelper::getErrorResponse('USER_NOT_EXISTS');
        }
        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return ResponseHelper::getResponse([
                    'token' => JwtGenerator::generate($user),
                    'user_data' => $user
                ],
                __METHOD__
            );
        }
        // Bad Request response
        return ResponseHelper::getErrorResponse('USER_NOT_EXISTS');
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user
     * @return mixed
     */
    public function register(User $user) {

        $keepOnly = array_merge(User::VALIDATION_RULES, ['name'  => 'required|max:150']);
        $this->validate($this->request, $keepOnly);                
        $postFields = $this->processRequest($keepOnly);
        
        $userExists = User::where('email', $postFields['email'])->first();
        if ($userExists) {
            return ResponseHelper::getErrorResponse('USER_EXISTS');
        }
        $postFields['password'] = password_hash($postFields['password'], PASSWORD_DEFAULT);
        
        $createdUser = $user->saveUser($postFields);        
        return ResponseHelper::getResponse([
                'token' => JwtGenerator::generate($createdUser),
                'user_data' => $createdUser
            ],
            __METHOD__
        );
    }
}
