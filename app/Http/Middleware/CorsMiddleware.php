<?php
 
namespace App\Http\Middleware;
 
use Closure;
 
class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
 
        $response->header(
            'Access-Control-Allow-Headers',
            'Access-Control-Allow-Origin, Access-Control-Allow-Methods, Accept, Content-Type, Authorization'
        );
        $response->header(
            'Access-Control-Allow-Origin',
            '*'
        );
        $response->header(
            'Access-Control-Allow-Methods',
            'POST, GET, OPTIONS, PUT'
        );

        return $response;
    }
}
