<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    const VALIDATION_RULES = [
        'email'     => 'required|email|max:150',
        'password'  => 'required'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at'
    ];

    /**
     * Save user
     * 
     * @param  array $params
     * @return boolean 
     */
    public function saveUser($params)
    {       
        $params['created_at'] = Carbon::now()->toDateTimeString();
        $id = DB::table('users')->insertGetId($params);
        if (!$id) {
            return false;
        }
        return new User(
            array_merge($params, ['id' => $id])
        );
    }
}
